var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    postcode: {type: Number, required: true},
    gemeente: {type: String, required: true}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Gemeente', schema);