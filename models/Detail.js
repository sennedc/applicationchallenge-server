var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    naam: {type: String, required: true},
    email: {type: String, required: true},
    adres: {type: String, required: true},
    btwnummer: {type: String, required: false},
    telefoon: {type: String, required: true}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Detail', schema);