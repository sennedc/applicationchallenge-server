var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    prijs: {type: Number, required: true},
    isActief: {type: Boolean, required: true, default: true},
    zaal: {type: Schema.Types.ObjectId, ref:'Zaal'},
    soortPrijs: {type: Schema.Types.ObjectId, ref:'SoortPrijs'}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Prijs', schema);