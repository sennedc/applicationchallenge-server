var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    vertaling: { type: String, required: true },
    type: {type: Number, required: true}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Status', schema);

