var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    naam: {type: String, required: true},
    cultureCode: {type: String, required: true, unique: true}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Taal', schema);

