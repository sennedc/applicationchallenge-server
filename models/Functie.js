var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    role: {type: String, required: true},
    titel: {type: String, required: true}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Functie', schema);