var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    datum: {type: Date, required: true},
    startuur: {type: Number, required: true}, // In minuten
    duur: {type: Number, required: true}, // In minuten
    datumReservatie: {type: Date, required: true},
    motivatie: {type: String, required: true},
    isBetaald: {type: Boolean, required: true, default: false},
    datumBetaling: {type: Date},
    statusWijzigingen: [{type: Schema.Types.ObjectId, ref:'StatusWijziging'}],
    prijs: {type: Number, required: true},
    zalen: [{type: Schema.Types.ObjectId, ref:'Zaal'}],
    detail: {type: Schema.Types.ObjectId, ref: 'Detail'}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Reservatie', schema);