var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    adres: {type: String, required: true},
    naam: {type: String, required: true},
    isActief: {type: Boolean, required: true, default: true},
    gemeente: {type: Schema.Types.ObjectId, ref:'Gemeente'}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Gebouw', schema);

