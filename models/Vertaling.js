var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    vertaling: {type: String, required: true},
    vertalingId: {type: String, required: true},
    taal: {type: Schema.Types.ObjectId, ref: 'Taal'}

});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Vertaling', schema);