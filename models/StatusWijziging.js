var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    datum: {type: Date, required: true},
    gebruiker: { type: Schema.Types.ObjectId, ref: 'Gebruiker' },
    status: {type: Schema.Types.ObjectId, ref: 'Status' }
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('StatusWijziging', schema);