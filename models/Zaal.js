var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    naam: {type: String, required: true},
    breedte: {type: Number, required: true},
    lengte: {type: Number, required: true},
    isActief: {type: Boolean, required: true, default: true},
    gebouw: {type: Schema.Types.ObjectId, ref:'Gebouw'},
    zaalAttributen: [{type: Schema.Types.ObjectId, ref:'ZaalAttribuut'}],
    deelZalen: [{type: Schema.Types.ObjectId, ref:'Zaal'}],
    vertaling: {type: String}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Zaal', schema);