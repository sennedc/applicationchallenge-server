var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    email: {type: String, required: true, unique: true},
    wachtwoord: {type: String, required: true},
    functie: {type: Schema.Types.ObjectId, ref:'Functie'}
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('Gebruiker', schema);