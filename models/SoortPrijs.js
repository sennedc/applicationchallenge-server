var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    soort: {type: String, required: true},
    isActief: {type: Boolean, required: true, default: true},

});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
schema.plugin(deepPopulate, {});

module.exports = mongoose.model('SoortPrijs', schema);