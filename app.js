var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');
var appRoutes = require('./routes/app');

mongoose.connect('mongodb://javachallenge:javachallenge@ds117956.mlab.com:17956/javachallenge', {
    useMongoClient: true
});

var app = express();

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// view engine setup
//app.set('views', path.join(__dirname, 'public'));
//app.engine('html', require('ejs').renderFile);
//app.set('view engine', 'html');

app.use(logger('dev'));
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, PUT, DELETE, OPTIONS');
    next();
});

app.use('/test',
    async (req, res) => {
        try {
            res.json();
        } catch (err) {
            res.status(500).json(err);
        }
    });

app.use('/', appRoutes);

// catch 404 and forward to helper handler
app.use(function (req, res, next) {
    res.status(404).json({ message: 'Not found!' });
});


app.use(function (err, req, res, next) {
    console.log(err);
    res.status(err.code == undefined ? 500 : err.code).json(err);
});


module.exports = app;
