var Detail = require('../models/Detail');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleDetails = async (query) => {
    return await httpGetHelper.find(query, Detail);
};

var getDetailVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Detail);
};

var maakDetail = async (detail) => {
    var detailModel = new Detail({
        naam: detail.naam,
        email: detail.email,
        adres: detail.adres,
        btwnummer: detail.btwnummer,
        telefoon: detail.telefoon
    });
    return await detailModel.save();
};

module.exports.getAlleDetails = getAlleDetails;
module.exports.getDetailVoorId = getDetailVoorId;
module.exports.maakDetail = maakDetail;