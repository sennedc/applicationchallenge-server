var Status = require('../models/Status');
var vertalingDomain = require('./vertaling.domain');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleStatussen = async (query) => {
    return await httpGetHelper.find(query, Status);
};

var getStatusVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Status);
};

var maakStatus = async (status) => {
    var vertaling = await vertalingDomain.maakVertalingen(status.vertalingen);

    var statusModel = new Status({
        vertaling: vertaling,
        type: status.type
    });
    return await statusModel.save();
};

module.exports.getAlleStatussen = getAlleStatussen;
module.exports.getStatusVoorId = getStatusVoorId;
module.exports.maakStatus = maakStatus;
