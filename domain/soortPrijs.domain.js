var SoortPrijs = require('../models/SoortPrijs');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleSoortPrijzen = async(query) => {
    return await httpGetHelper.find(query, SoortPrijs);
};

var getSoortPrijsVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, SoortPrijs);

};

var maakSoortPrijs = async (object) => {
    if (object == undefined)
        throw { code: 400, message: 'Invalide soortprijs' };
    var soortprijs = new SoortPrijs({
        soort: object.soort
    });

    return await soortprijs.save();
};

var verwijderSoortPrijs = async (id) => {
    var soortPrijs = await SoortPrijs.findById(id);
    soortPrijs.isActief = false;

    return await soortPrijs.save();
};

module.exports.getAlleSoortPrijzen = getAlleSoortPrijzen;
module.exports.getSoortPrijsVoorId = getSoortPrijsVoorId;
module.exports.maakSoortPrijs = maakSoortPrijs;
module.exports.verwijderSoortPrijs = verwijderSoortPrijs;