var Reservatie = require('../models/Reservatie');
var Status = require('../models/Status');
var zalenDomain = require('./zaal.domain');
var detailDomein = require('./detail.domain');
var statusWijzigingDomain = require('./statuswijziging.domain');
var moment = require('moment');
var httpGetHelper = require('../helper/httpGet.helper');
var emailHelper = require('../helper/email.helper');

var getAlleReservaties = async (query) => {
    return await httpGetHelper.find(query, Reservatie);
};

var getReservatieVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Reservatie);
};

var maakReservatie = async (reservatie) => {
    var zalenPrijs = await zalenDomain.getTotalePrijsVoorZalen(reservatie.zalen);
    var startStatus = await Status.findOne({ type: 1 }, '_id');
    var statusWijziging = await statusWijzigingDomain.maakStatusWijziging({ status: startStatus._id, type: 1 });

    var reservatieModel = new Reservatie({
        datum: moment().utc().toDate(),
        startuur: reservatie.startuur,
        duur: reservatie.duur,
        datumReservatie: reservatie.datumReservatie,
        motivatie: reservatie.motivatie,
        prijs: zalenPrijs,
        zalen: reservatie.zalen,
        statusWijzigingen: [statusWijziging._id],
        detail: (await detailDomein.maakDetail(reservatie.detail))._id
    });
    var result = await reservatieModel.save();
    await emailHelper.notificatieEmail(statusWijziging);
    return result;
};

var verwijderReservatie = async (id) => {
    return await updateReservatie(id, { isActief: false });
};

var updateReservatie = async (id, properties) => {
    return await Reservatie.findOneAndUpdate({ _id: id }, properties, { new: true, runValidators: true });
};

var overschrijfReservatie = async (id, reservatie) => {
    return await Reservatie.findOneAndUpdate({ _id: id }, reservatie, { overwrite: true, new: true, runValidators: true });
};

var wijzigStatusReservatie = async (reservatieId, statusWijziging) => {
    var statusWijzigingModel = await statusWijzigingDomain.maakStatusWijziging({
        gebruiker: statusWijziging.gebruiker,
        status: statusWijziging.status,
        type: statusWijziging.type
    });

    var reservatie = await Reservatie.findOneAndUpdate({ _id: reservatieId }, { $push: { statusWijzigingen: statusWijzigingModel._id } }, { new: true });

    await emailHelper.notificatieEmail(statusWijzigingModel);

    return reservatie;
};


module.exports.getAlleReservaties = getAlleReservaties;
module.exports.getReservatieVoorId = getReservatieVoorId;
module.exports.maakReservatie = maakReservatie;
module.exports.verwijderReservatie = verwijderReservatie;
module.exports.updateReservatie = updateReservatie;
module.exports.overschrijfReservatie = overschrijfReservatie;
module.exports.wijzigStatusReservatie = wijzigStatusReservatie;