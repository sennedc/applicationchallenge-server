var Gemeente = require('../models/Gemeente');
var httpGetHelper = require('../helper/httpGet.helper');
var stringHelper = require('../helper/string.helper');

const gemeenteSortRegex = /[A-z]/;

var getAlleGemeenten = async (query) => {
    if (query.gemeente != undefined) 
        query.gemeente = { $regex: new RegExp(query.gemeente, 'i') };
    var gemeenten = await httpGetHelper.find(query, Gemeente, null, { gemeente: 1 });
    var firstLetterIndex = -1;
    for (let i = 0; i < gemeenten.length; i++) {
        let gemeente = gemeenten[i];
        if (firstLetterIndex === -1 && gemeenteSortRegex.test(gemeente.gemeente.substr(0, 1)))
            firstLetterIndex = i;
        gemeente.gemeente = stringHelper.toTitleCase(gemeente.gemeente);
    }
    gemeenten = gemeenten.splice(firstLetterIndex, gemeenten.length).concat(gemeenten.slice(0, firstLetterIndex));
    return gemeenten;
};

var getGemeenteVoorId = async (id, query) => {
    var gemeente = await httpGetHelper.findOne(id, query, Gemeente);
    gemeente.gemeente = stringHelper.toTitleCase(gemeente.gemeente);
    return gemeente;
};

var maakGemeente = async (objecten) => {
    if (objecten == undefined)
        throw { code: 400, message: 'Invalide gemeente' };

    for (let i = 0; i < objecten.length; i++) {
        let gemeente = new Gemeente({
            postcode: objecten[i].postcode,
            gemeente: objecten[i].gemeente
        });
        await gemeente.save();
    }
    return {};
};
module.exports.getAlleGemeenten = getAlleGemeenten;
module.exports.getGemeenteVoorId = getGemeenteVoorId;
module.exports.maakGemeente = maakGemeente;