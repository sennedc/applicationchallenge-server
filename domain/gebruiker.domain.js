var Gebruiker = require('../models/Gebruiker');
var httpGetHelper = require('../helper/httpGet.helper');
var jwt = require('jsonwebtoken');

var getAlleGebruikers = async (query) => {
    delete query.wachtwoord;

    var gebruikers = await httpGetHelper.find(query, Gebruiker);
    gebruikers.forEach(gebruiker => gebruiker.wachtwoord = "");
    return gebruikers;
};

var getGebruikerVoorId = async (id, query) => {
    var gebruiker = await httpGetHelper.findOne(id, query, Gebruiker);
    gebruiker.wachtwoord = "";
    return gebruiker;
};

var login = async (email, wachtwoord) => {
    query = {include: 'functie', email: email};
    var gebruiker = await httpGetHelper.findOne(null, query, Gebruiker);

    if(wachtwoord != gebruiker.wachtwoord){throw 401;}

    var token = jwt.sign({gebruiker: gebruiker}, 'canYouFindMe', {expiresIn: 7200});
    return token;

};


var maakGebruiker = async (gebruiker) => {
    var gebruikerModel = new Gebruiker({
        email: gebruiker.email,
        wachtwoord: gebruiker.wachtwoord,
        functie: gebruiker.functie
    });
    return await gebruikerModel.save();
};

module.exports.getAlleGebruikers = getAlleGebruikers; 
module.exports.getGebruikerVoorId = getGebruikerVoorId;
module.exports.maakGebruiker = maakGebruiker;
module.exports.login = login;