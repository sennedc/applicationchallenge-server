var Taal = require('../models/Taal');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleTalen = async (query) => {
    return await httpGetHelper.find(query, Taal);
};

var getTaalVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Taal);
};

var maakTaal = async (taal) => {
    if (taal == undefined)
        throw { code: 400, message: 'Invalide taal' };
    var taalModel = new Taal({
        naam: taal.naam,
        cultureCode: taal.cultureCode
});
    return await taalModel.save();
};

module.exports.getAlleTalen = getAlleTalen;
module.exports.getTaalVoorId = getTaalVoorId;
module.exports.maakTaal = maakTaal;