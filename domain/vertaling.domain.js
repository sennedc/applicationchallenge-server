var Vertaling = require('../models/Vertaling');
var uuid = require('uuid/v1');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleVertalingen = async (query) => {
    return await httpGetHelper.find(query, Vertaling);
};

var getVertalingVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Vertaling);
};

var getVertalingenVoorVertaalId = async (id) => {
    return await Vertaling.find({ vertalingId: id });
};

var maakVertaling = async (vertaling) => {
    var vertalingModel = new Vertaling({
        vertaling: vertaling.vertaling,
        vertalingId: vertaling.vertalingId,
        taal: vertaling.taal
    });
    return await vertalingModel.save();
};

var maakVertalingen = async (vertalingen) => {
    var result = [];
    var vertalingId = uuid();
    vertalingen.forEach(async vertaling => {
        vertaling.vertalingId = vertalingId;
        result.push(await maakVertaling(vertaling)); 
    });
    return vertalingId;
};

var verwijderVertaling = async (id) => {
    return await Vertaling.findOneAndRemove(id);
};

var updateVertaling = async (id, properties) => {
    return await Vertaling.findOneAndUpdate({ _id: id }, properties, { new: true, runValidators: true });
};

var overschrijfVertaling = async (id, vertaling) => {
    return await Vertaling.findOneAndUpdate({ _id: id }, vertaling, { overwrite: true, new: true, runValidators: true });
};

module.exports.getAlleVertalingen = getAlleVertalingen;
module.exports.getVertalingVoorId = getVertalingVoorId;
module.exports.maakVertaling = maakVertaling;
module.exports.maakVertalingen = maakVertalingen;
module.exports.verwijderVertaling = verwijderVertaling;
module.exports.updateVertaling = updateVertaling;
module.exports.overschrijfVertaling = overschrijfVertaling;
module.exports.getVertalingenVoorVertaalId = getVertalingenVoorVertaalId;