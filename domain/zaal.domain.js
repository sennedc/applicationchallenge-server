var Zaal = require('../models/Zaal');
var Reservatie = require('../models/Reservatie');
var SoortPrijs = require('../models/SoortPrijs');
var vertalingDomain = require('./vertaling.domain');
var reservatieDomein = require('./reservatie.domain');
var prijsDomain = require('./prijs.domain');
var httpGetHelper = require('../helper/httpGet.helper');
var moment = require('moment');
var jwt = require('jsonwebtoken');
var emailHelper = require('../helper/email.helper');

var getAlleZalen = async (query) => {
    return await httpGetHelper.find(query, Zaal);
};

var getZaalVoorId = async (id) => {
    return await httpGetHelper.findOne(id, query, Zaal);
};

var getBeschikbareDataVoorZaalEnMaand = async (id, year, month) => {
    var eersteVanDeMaand = moment(new Date(year, month, 1));
    var deelZalen = (await Zaal.findById(id, '-_id deelZalen')).deelZalen;
    var superZalen = await Zaal.find({ deelZalen: id, isActief: true });
    var reservaties = await Reservatie.find({ zalen: { $in: deelZalen.concat(superZalen).concat([id]) }, datumReservatie: { $gte: eersteVanDeMaand.toDate(), $lt: eersteVanDeMaand.add(eersteVanDeMaand.daysInMonth(), 'days').toDate() } }).deepPopulate('statusWijzigingen.status');
    var dataBezet = [];

    // Opvullen data
    for (let i = 0; i < eersteVanDeMaand.daysInMonth(); i++) {
        dataBezet.push({
            beschikbaar: true,
            reservaties: []
        });
    }
    for (let i = 0; i < reservaties.length; i++) {
        let isGeannuleerd = false;
        let isZeker = false;
        for (let k = 0; k < reservaties[i].statusWijzigingen.length; k++) {
            isZeker = isZeker || reservaties[i].statusWijzigingen[k].status.type === 2;
            if (reservaties[i].statusWijzigingen[k].status.type === 3) {
                isGeannuleerd = true;
                break;
            }
        }
        if (isGeannuleerd) continue;

        dataBezet[moment(reservaties[i].datumReservatie).date() - 1].reservaties.push({
            startuur: reservaties[i].startuur,
            duur: reservaties[i].duur,
            isZeker: isZeker,
            reservatie: reservaties[i]._id
        });
    }
    return dataBezet;
};

var maakZaal = async (object) => {
    var vertaling = await vertalingDomain.maakVertalingen(object.vertalingen);

    var zaal = await new Zaal({
        naam: object.naam,
        breedte: object.breedte,
        lengte: object.lengte,
        gebouw: object.gebouw,
        zaalAttributen: object.zaalAttributen,
        deelZalen: object.deelZalen,
        vertaling: vertaling
    }).save();

    var prijs = await prijsDomain.maakPrijs({
        zaal: zaal._id,
        soortPrijs: SoortPrijs.findOne({})._id,
        prijs: object.prijs
    });

    return zaal;
};

var verwijderZaal = async (id, token) => {
    var zaal = await Zaal.findById(id);

    let query = {zaal: id}
    var reservaties = await httpGetHelper.find(query, Reservatie);

    //var vandaag = moment().utc().toDate();

    for(let reservatie of reservaties){
        //if(reservatie.datum.time() > vandaag.time()){
            reservatieDomein.wijzigStatusReservatie(reservatie._id, {gebruiker: jwt.decode(token).gebruiker, status: "5a18209665746426048be57b", type: 3});
        //}
    }

    zaal.isActief = false;
    return await zaal.save();
};

var overschrijfZaal = async (object) => {
    var zaal = await Zaal.findById(object.id);

    vertalingDomain.overschrijfVertaling(zaal.vertaling, object.vertaling);

    zaal.breedte = object.breedte;
    zaal.lengte = object.lengte;
    zaal.gebouw = object.gebouw;
    zaal.zaalAttributen = object.zaalAttributen;
    zaal.deelZalen = object.deelZalen;
    zaal.naam = object.naam;
    zaal.vertaling = object.vertaling;

    if(object.prijsId != null){
        prijsDomain.updatePrijs(object.prijsId, {prijs: object.prijs});
    }

    return await zaal.save();
};

var getTotalePrijsVoorZalen = async (zalen) => {
    var totalePrijs = 0;
    for (var i = 0; i < zalen.length; i++) {
        totalePrijs += await prijsDomain.getPrijsVoorZaal(zalen[i]);
    }
    return totalePrijs;
};

var activeerZaal = async (id) => {
    return await Zaal.findOneAndUpdate({ _id: id }, {isActief: true}, { new: true, runValidators: true });
}

module.exports.getAlleZalen = getAlleZalen;
module.exports.getZaalVoorId = getZaalVoorId;
module.exports.maakZaal = maakZaal;
module.exports.verwijderZaal = verwijderZaal;
module.exports.overschrijfZaal = overschrijfZaal;
module.exports.getTotalePrijsVoorZalen = getTotalePrijsVoorZalen;
module.exports.getBeschikbareDataVoorZaalEnMaand = getBeschikbareDataVoorZaalEnMaand;
module.exports.activeerZaal = activeerZaal;