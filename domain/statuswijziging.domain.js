var StatusWijziging = require('../models/StatusWijziging');
var moment = require('moment');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleStatusWijzigingen = async (query) => {
    return await httpGetHelper.find(query, StatusWijziging);
};

var getStatusWijzigingVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, StatusWijziging);
};

var maakStatusWijziging = async (statusWijziging) => {
    var statusWijzigingModel = new StatusWijziging({
        datum: moment().utc().toDate(),
        gebruiker: statusWijziging.gebruiker,
        status: statusWijziging.status,
        type: statusWijziging.type
    });

    statusWijziging = await statusWijzigingModel.save();
    return statusWijziging;
};

module.exports.getAlleStatusWijzigingen = getAlleStatusWijzigingen;
module.exports.getStatusWijzigingVoorId = getStatusWijzigingVoorId;
module.exports.maakStatusWijziging = maakStatusWijziging;