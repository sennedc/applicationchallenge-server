var ZaalAttribuut = require('../models/ZaalAttribuut');
var httpGetHelper = require('../helper/httpGet.helper');


var getAlleZaalAttributen = async (query) => {
    return await httpGetHelper.find(query, ZaalAttribuut);
};

var getZaalAttribuutVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, ZaalAttribuut);
};

var maakZaalAttribuut = async (zaalAttribuut) => {
    var zaalAttribuutModel = new ZaalAttribuut({
        titel: zaalAttribuut.titel
    });
    return await zaalAttribuutModel.save();
};

var verwijderZaalAttribuut = async (id) => {
    return await updateZaalAttribuut(id, { isActief: false });
};

var updateZaalAttribuut = async (id, properties) => {
    return await ZaalAttribuut.findOneAndUpdate({ _id: id }, properties, { new: true, runValidators: true });
};

var overschrijfZaalAttribuut = async (id, zaalAttribuut) => {
    return await ZaalAttribuut.findOneAndUpdate({ _id: id }, zaalAttribuut, { overwrite: true, new: true, runValidators: true });
};


module.exports.getAlleZaalAttributen = getAlleZaalAttributen;
module.exports.getZaalAttribuutVoorId = getZaalAttribuutVoorId;
module.exports.maakZaalAttribuut = maakZaalAttribuut;
module.exports.verwijderZaalAttribuut = verwijderZaalAttribuut;
module.exports.updateZaalAttribuut = updateZaalAttribuut;
module.exports.overschrijfZaalAttribuut = overschrijfZaalAttribuut;