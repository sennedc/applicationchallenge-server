var Gebouw = require('../models/Gebouw');
var Zaal = require('../models/Zaal');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleGebouwen = async(query) => {
    return await httpGetHelper.find(query, Gebouw);
};

var getGebouwVoorId = async (id, query) => {

    return await httpGetHelper.findOne(id, query, Gebouw);

};


var maakGebouw = async (object) => {
    var gebouw = new Gebouw({
        adres: object.adres,
        gemeente: object.gemeente,
        naam: object.naam
    });

    return await gebouw.save();
};

var verwijderGebouw = async (id, token) => {
    let query = {gebouw: id};
    var zalen = await zaalDomein.getAlleZalen(query);

    for(let zaal of zalen){
        zaalDomein.verwijderZaal(zaal._id, token);
    }

    var gebouw = await Gebouw.findById(id);
    gebouw.isActief = false;

    return await gebouw.save();
};

var updateGebouw = async (id, properties) => {
    return await Gebouw.findOneAndUpdate({ _id: id }, properties, { new: true, runValidators: true });
};

var overschrijfGebouw = async (id, gebouw) => {
    return await Gebouw.findOneAndUpdate({ _id: id }, gebouw, { overwrite: true, new: true, runValidators: true });
};

module.exports.getAlleGebouwen = getAlleGebouwen;
module.exports.getGebouwVoorId = getGebouwVoorId;
module.exports.maakGebouw = maakGebouw;
module.exports.verwijderGebouw = verwijderGebouw;
module.exports.updateGebouw = updateGebouw;
module.exports.overschrijfGebouw = overschrijfGebouw;