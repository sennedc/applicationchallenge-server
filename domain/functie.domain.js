var Functie = require('../models/Functie');
var httpGetHelper = require('../helper/httpGet.helper');

var getAlleFuncties = async (query) => {
    return await httpGetHelper.find(query, Functie);
};

var getFunctieVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Functie);
};

var maakFunctie = async (functie) => {
    var functieModel = new Functie({
        role: functie.role,
        titel: functie.titel
    });
    return await functieModel.save();
};

module.exports.getAlleFuncties = getAlleFuncties;
module.exports.getFunctieVoorId = getFunctieVoorId;
module.exports.maakFunctie = maakFunctie;