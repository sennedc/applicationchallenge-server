var Prijs = require('../models/Prijs');
var httpGetHelper = require('../helper/httpGet.helper');


var getAllePrijzen = async(filter) => {
    return await httpGetHelper.find(filter, Prijs);
};

var getPrijsVoorId = async (id, query) => {
    return await httpGetHelper.findOne(id, query, Prijs);

};

var maakPrijs = async (prijs) => {
    return await new Prijs({
        prijs: prijs.prijs,
        zaal: prijs.zaal,
        soortPrijs: prijs.soortPrijs
    }).save();
};

var getPrijsVoorZaal = async (zaal) => {
    var prijs = await Prijs.findOne({ zaal: zaal }, '-_id prijs');
    return prijs == undefined ? 0 : prijs.prijs;
};

var updatePrijs = async (id, properties) => {
    return await Prijs.findOneAndUpdate({ _id: id }, properties, { new: true, runValidators: true });
};

var overschrijfPrijs = async (id, properties) => {
    return await Prijs.findOneAndUpdate({ _id: id }, properties, { overwrite: true, new: true, runValidators: true });
};

module.exports.getAllePrijzen = getAllePrijzen;
module.exports.getPrijsVoorId = getPrijsVoorId;
module.exports.maakPrijs = maakPrijs;
module.exports.getPrijsVoorZaal = getPrijsVoorZaal;
module.exports.updatePrijs = updatePrijs;
module.exports.overschrijfPrijs = overschrijfPrijs;