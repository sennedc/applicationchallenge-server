if (!('toJSON' in Error.prototype))
    Object.defineProperty(Error.prototype, 'toJSON', {
        value: function () {
            var alt = {};

            Object.getOwnPropertyNames(this).forEach(function (key) {
                alt[key] = this[key];
            }, this);

            return alt;
        },
        configurable: true,
        writable: true
    });

var mongooseErrorToResult = (err, message) => {
    console.log(err);
    var result = {};
    if (err.name != undefined && err.name === 'ValidationError') {
        result.error = 'Invalide gegevens';
    }
    if (message != undefined)
        result.message = message;
    return err;
};

module.exports.mongooseErrorToResult = mongooseErrorToResult;