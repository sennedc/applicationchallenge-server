var Reservatie = require('../models/Reservatie');
var Status = require('../models/Status');
var StatusWijziging = require('../models/StatusWijziging');
var Functie = require('../models/Functie');
var Gebruiker = require('../models/Gebruiker');
const sgMail = require('@sendgrid/mail');
const emailSender = 'lgucenter@lgu.be';
sgMail.setApiKey('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');


const testEmail = {
    to: 'senne.deceulaer@allphi.eu',
    from: emailSender,
    subject: 'Sending with SendGrid is Fun',
    text: 'and easy to do anywhere, even with Node.js',
    html: '<strong>and easy to do anywhere, even with Node.js</strong>'
};

var testMail = async () => {
    return await sgMail.send(testEmail);
};


var stuurMail = async (to, subject, body) => {
    var email = {
        from: emailSender,
        to: to,
        subject: subject,
        text: body
    };
    return await sgMail.send(email);
};

var notificatieAdmin = async () => {
    var adminFunctie = await Functie.findOne({ role: "admin" });
    var admin = await Gebruiker.findOne({ functie: adminFunctie });

    const onderwerp = "Nieuwe reservatie van een zaal";

    const inhoud = "Beste,\nEr is een nieuwe reservatie aangemaakt. U kan deze nu bekijken op de webtoepassing.\nMvg,\nHet LGU-Team!";

    return await stuurMail(admin.email, onderwerp, inhoud);
}

var notificatieEmail = async (statuswijziging) => {
    var reservatie = await Reservatie.findOne({ statusWijzigingen: { $all: [statuswijziging._id] } }).populate('detail');
    statuswijziging = await StatusWijziging.findById(statuswijziging._id).populate('status');

    const onderwerp = "Status van jou reservatie!";
    let inhoud = 'Placeholder inhoud';

    if (statuswijziging.status.type === 1) {
        inhoud = "Beste,\nWij hebben uw aanvraag goed ontvangen en zullen deze zo snel mogelijk verwerken. U krijgt van ons een bevestiging per mail indien uw reservatie wordt goedgekeurd.\nMet vriendelijke groeten,\nHet Let's Go Urban team!";
        await notificatieAdmin();
    } else if (statuswijziging.status.type === 2) {
        inhoud =
            "Beste,\nOnlangs heeft u een zaal gereserveerd via onze website. Deze aanvraag is door een van onze medewerkers behandeld en is hierbij bevestigd. Indien u de reservatie alsnog wilt annuleren, gelieve dan contact op te nemen." +
            "\nIndien er toch nog wijzigingen zouden plaatsvinden wordt u hiervan steeds op de hoogte gebracht.\nMet vriendelijke groeten,\nHet Let's Go Urban team!";
    }else if (statuswijziging.status.type === 3) {
        inhoud =
            "Beste,\nOnlangs heeft u een zaal gereserveerd via onze website. Helaas moeten wij u mededelen dat we de zaal niet voor u kunnen reserveren. " +
            "Indien u hierover meer informatie wenst kan u steeds contact met ons opnemen via de website.\nMet vriendelijke groeten,\nHet Let's Go Urban team!";
    }

    await stuurMail(reservatie.detail.email, onderwerp, inhoud);
};

module.exports.notificatieEmail = notificatieEmail;
module.exports.testMail = testMail;