var jwt = require('jsonwebtoken');
var errorHelper = require('./error.helper');

var controleerGebruikerIsIngelogd = (token) => {
    jwt.verify(token, 'canYouFindMe');
};

var controleerGebruikerIsAdmin = (token) => {
    controleerGebruikerIsIngelogd(token);
    if (jwt.decode(token).gebruiker.functie.role !== "admin")
        throw Error('User does not have enough privileges');
};

var zetAlsIngelogdeGebruikerRoute = (router) => {
    router.use('/', (req, res, next) => {
        try {
            controleerGebruikerIsIngelogd(req.query.token);
            next();
        }
        catch (err) {
            res.status(401).json(errorHelper.mongooseErrorToResult(err));
        }
    });
};

var zetAlsAdminRoute = (router) => {
    router.use('/', (req, res, next) => {
        try {
            controleerGebruikerIsAdmin(req.query.token);
            next();
        }
        catch (err) {
            res.status(401).json(errorHelper.mongooseErrorToResult(err));
        }
    });
};

module.exports.controleerGebruikerIsIngelogd = controleerGebruikerIsIngelogd;
module.exports.controleerGebruikerIsAdmin = controleerGebruikerIsAdmin;
module.exports.zetAlsIngelogdeGebruikerRoute = zetAlsIngelogdeGebruikerRoute;
module.exports.zetAlsAdminRoute = zetAlsAdminRoute;

