var Vertaling = require('../models/Vertaling');
var Taal = require('../models/Taal');

const cultureCode = 'nl';

var find = async (query, schema) => {
    var huidigeTaal = (await Taal.findOne({ cultureCode: cultureCode }, '_id'))._id;
    var filters = {};
    var includes = "";
    var sorts = [];
    var vertaling = false;

    for (var propt in query) {
        if (query.hasOwnProperty(propt)) {
            if (propt == "include") {
                if (query[propt] instanceof Array) {
                    for (let i = 0; i < query[propt].length; i++)
                        /(.*)vertaling/ig.test(query[propt][i]) ? vertaling = true : includes += `${query[propt][i]} `;

                } else if (!/(.*)vertaling/ig.test(query[propt])) {
                    includes += query[propt] + " ";
                }
                else vertaling = true;
            }
            else if (propt == "sort") {
                var object = [];
                object.push(query[propt]);
                object.push(1);
                sorts.push(object);
            }
            else if (propt != "token") {
                filters[propt] = query[propt];
            }
        }
    }
    console.log(sorts);
    var result = await schema.find(filters).deepPopulate(includes).sort(sorts);

    if (vertaling)
        for (var i = 0; i < result.length; i++)
            await vertaalAlles(result[i], huidigeTaal);

    return result;
};

var findOne = async (id, query, schema) => {
    var huidigeTaal = (await Taal.findOne({ cultureCode: cultureCode }, '_id'))._id;
    var filters = id == null ? {} : { _id: id };
    var includes = "";
    var sorts = [];
    var vertaling = false;
    for (var propt in query) {
        if (query.hasOwnProperty(propt)) {
            if (query[propt] instanceof Array) {
                for (let i = 0; i < query[propt].length; i++)
                    /(.*)vertaling/ig.test(query[propt][i]) ? vertaling = true : includes += `${query[propt][i]} `;

            } else if (!/(.*)vertaling/ig.test(query[propt])) {
                includes += query[propt] + " ";
            }
            else if (propt == "sort") {
                var object = [];
                object.push(query[propt]);
                object.push(1);
                sorts.push(object);
            }
            else if (propt != "token") {
                filters[propt] = query[propt];
            }
        }
    }
    var result = await schema.findOne(filters).deepPopulate(includes).sort(sorts);

    if (vertaling) {
        await vertaalAlles(result, huidigeTaal);
        //result.vertaling = (await Vertaling.findOne({ vertalingId: result.vertaling, taal: huidigeTaal }, '-_id vertaling')).vertaling;
    }

    return result;
};



var vertaalAlles = async (obj, huidigeTaal) => {
    if (obj == undefined)
        return;
    if (Array.isArray(obj))
        for (let i = 0; i < obj.length; i++)
            await vertaalAlles(obj[i], huidigeTaal);
    else {
        await vertaal(obj, huidigeTaal);
        if (obj.schema != undefined)
            for (var propt in obj.schema.paths) {
                var value = obj[propt];
                if (value == undefined)
                    continue;
                if (Array.isArray(value))
                    for (let i = 0; i < value.length; i++)
                        await vertaalAlles(value[i], huidigeTaal);
                else
                    await vertaalAlles(value, huidigeTaal);
            }
    }
};

var vertaal = async (obj, huidigeTaal) => {
    if (obj.vertaling == undefined)
        return;
    var vertaling = await Vertaling.findOne({ vertalingId: obj.vertaling, taal: huidigeTaal }, '-_id vertaling');
    if (vertaling != null)
        obj.vertaling = vertaling.vertaling;
};

module.exports.find = find;
module.exports.findOne = findOne;