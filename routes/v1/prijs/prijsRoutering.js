var express = require('express');
var domain = require('../../../domain/prijs.domain');
var error = require('../../../helper/error.helper');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async function (req, res) {
    try {
        res.json(await domain.getAllePrijzen(req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.get('/:id', async function (req, res) {
    try {
        res.json(await domain.getPrijsVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.patch('/:id', async (req, res) => {
    try {
        res.json(await domain.updatePrijs(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

module.exports = router;