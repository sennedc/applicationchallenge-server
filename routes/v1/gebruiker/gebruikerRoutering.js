var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/gebruiker.domain');
var router = express.Router();

router.get('/', async (req, res) => {
    try{
        res.json(await domain.getAlleGebruikers(req.query));
    }
    catch(err){
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getGebruikerVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.post('/login', async (req, res) => {
   try{
       var token = await domain.login(req.body.email, req.body.wachtwoord);
       res.json(token);
   }
   catch(err){
       res.status(401).json(errorHelper.mongooseErrorToResult(err));
   }
});

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakGebruiker(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

module.exports = router;