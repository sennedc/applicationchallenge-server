var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/status.domain');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async (req, res) => {
    try {
        res.json(await domain.getAlleStatussen(req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getStatusVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakStatus(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});


module.exports = router;
