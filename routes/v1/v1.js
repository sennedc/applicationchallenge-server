﻿var express = require('express');
var router = express.Router();

var gebruikerRoutering = require('./gebruiker/gebruikerRoutering');
var functieRoutering = require('./functie/functieRoutering');
var gebouwRoutering = require('./gebouw/gebouwRoutering');
var gemeenteRoutering = require('./gemeente/gemeenteRoutering');
var prijsRoutering = require('./prijs/prijsRoutering');
var reservatieRoutering = require('./reservatie/reservatieRoutering');
var statusRoutering = require('./status/statusRoutering');
var statusWijzigingRoutering = require('./statusWijziging/statusWijzigingRoutering');
var taalRoutering = require('./taal/taalRoutering');
var vertalingRoutering = require('./vertaling/vertalingRoutering');
var zaalRoutering = require('./zaal/zaalRoutering');
var zaalAttribuutRoutering = require('./zaalAttribuut/zaalAttribuutRoutering');
var soortPrijsRoutering = require('./soortPrijs/soortPrijsRoutering');
var detailRoutering = require('./detail/detailRoutering');

router.get('/', function(req, res) {
    res.send({ success: true, message: 'First test success' });
});

router.use('/detail', detailRoutering);
router.use('/gebruiker', gebruikerRoutering);
router.use('/functie', functieRoutering);
router.use('/gebouw', gebouwRoutering);
router.use('/gemeente', gemeenteRoutering);
router.use('/prijs', prijsRoutering);
router.use('/reservatie', reservatieRoutering);
router.use('/status', statusRoutering);
router.use('/statuswijziging', statusWijzigingRoutering);
router.use('/taal', taalRoutering);
router.use('/vertaling', vertalingRoutering);
router.use('/zaal', zaalRoutering);
router.use('/zaalattribuut', zaalAttribuutRoutering);
router.use('/soortPrijs', soortPrijsRoutering);

module.exports = router;
