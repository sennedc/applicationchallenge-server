var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/detail.domain');
var router = express.Router();

router.get('/', async (req, res) => {
    try {
        res.json(await domain.getAlleDetails(req.query));
    } catch (err) {
        res.functie(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getDetailVoorId(req.params.id, req.query));
    } catch (err) {
        res.functie(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakDetail(req.body));
    } catch (err) {
        res.functie(500).json(errorHelper.mongooseErrorToResult(err));
    }
});


module.exports = router;
