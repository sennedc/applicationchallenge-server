var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/reservatie.domain');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakReservatie(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsIngelogdeGebruikerRoute(router);

router.get('/', async (req, res) => {
    try {
        res.json(await domain.getAlleReservaties(req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getReservatieVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/:id/statuswijziging', async (req, res) => {
    try {
        res.json(await domain.wijzigStatusReservatie(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await domain.verwijderReservatie(req.params.id);
        res.status(204).json();
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.patch('/:id', async (req, res) => {
    try {
        res.json(await domain.updateReservatie(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.put('/:id', async (req, res) => {
    try {
        res.json(await domain.overschrijfReservatie(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});


module.exports = router;