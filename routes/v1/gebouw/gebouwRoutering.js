var express = require('express');
var domain = require('../../../domain/gebouw.domain');
var errorHelper = require('../../../helper/error.helper');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async function (req, res) {
    try {
        res.json(await domain.getAlleGebouwen(req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async function (req, res) {
    try {
        res.json(await domain.getGebouwVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

//POST
router.post('/', async function (req, res) {
    try {
        res.status(201).json(await domain.maakGebouw(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.patch('/:id', async (req, res) => {
    try {
        res.json(await domain.updateGebouw(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.put('/:id', async (req, res) => {
    try {
        res.json(await domain.overschrijfGebouw(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});


router.delete('/:id', async function (req, res) {
    try {
        res.status(204).json(await domain.verwijderGebouw(req.params.id, req.query.token));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

module.exports = router;
