var express = require('express');
var domain = require('../../../domain/soortPrijs.domain');
var error = require('../../../helper/error.helper');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async function (req, res) {
    try {
        res.json(await domain.getAlleSoortPrijzen(req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.get('/:id', async function (req, res) {
    try {
        res.json(await domain.getSoortPrijsVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

//POST
router.post('/', async function (req, res) {
    try {
        res.status(201).json(await domain.maakSoortPrijs(req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});


router.delete('/:id', async function (req, res, next) {
    try {
        res.status(204).json(await domain.verwijderSoortPrijs(req.params.id));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

module.exports = router;

