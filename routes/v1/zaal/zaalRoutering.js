var express = require('express');
var domain = require('../../../domain/zaal.domain');
var error = require('../../../helper/error.helper');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async function (req, res) {
    try {
        res.status(200).json(await domain.getAlleZalen(req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.get('/:id', async function (req, res) {
    try {
        res.status(200).json(await domain.getZaalVoorId(req.params.id, query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.get('/:id/beschikbaarheid/:jaar-:maand', async function (req, res) {
    try {
        res.status(200).json(await domain.getBeschikbareDataVoorZaalEnMaand(req.params.id, req.params.jaar, req.params.maand));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/', async function (req, res) {
    try {
        res.status(201).json(await domain.maakZaal(req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.put('/', async function (req, res) {
    try {
        res.status(200).json(await domain.overschrijfZaal(req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});


router.delete('/:id', async function (req, res, next) {
    try {
        res.status(204).json(await domain.verwijderZaal(req.params.id, req.query.token));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.patch('/:id/activeer', async function (req, res, next) {
    try{
        res.json(await domain.activeerZaal(req.params.id));
    }
    catch(err){
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

module.exports = router;
