var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/functie.domain');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async (req, res) => {
    try {
        res.json(await domain.getAlleFuncties(req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getFunctieVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakFunctie(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});


module.exports = router;
