var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/zaalattribuut.domain');
var securityHelper = require('../../../helper/security.helper');

var router = express.Router();

router.get('/', async (req, res) => {
    try {
        res.json(await domain.getAlleZaalAttributen(req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getZaalAttribuutVoorId(req.params.id, query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakZaalAttribuut(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await domain.verwijderZaalAttribuut(req.params.id);
        res.status(204).json();
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.patch('/:id', async (req, res) => {
    try {
        res.json(await domain.updateZaalAttribuut(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.put('/:id', async (req, res) => {
    try {
        res.json(await domain.overschrijfZaalAttribuut(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});


module.exports = router;