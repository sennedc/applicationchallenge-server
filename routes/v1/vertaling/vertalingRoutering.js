var express = require('express');
var errorHelper = require('../../../helper/error.helper');
var domain = require('../../../domain/vertaling.domain');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async (req, res) => {
    try {
        res.json(await domain.getAlleVertalingen(req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await domain.getVertalingVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/', async (req, res) => {
    try {
        res.status(201).json(await domain.maakVertaling(req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.patch('/:id', async (req, res) => {
    try {
        res.json(await domain.updateVertaling(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.put('/:id', async (req, res) => {
    try {
        res.json(await domain.overschrijfVertaling(req.params.id, req.body));
    } catch (err) {
        res.status(500).json(errorHelper.mongooseErrorToResult(err));
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await domain.verwijderVertaling(req.params.id);
        res.status(204).json();
    } catch (error) {
        res.status(500).json(errorHelper.mongooseErrorToResult(error));
    }
});


module.exports = router;
