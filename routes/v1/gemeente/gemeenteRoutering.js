var express = require('express');
var domain = require('../../../domain/gemeente.domain');
var error = require('../../../helper/error.helper');
var securityHelper = require('../../../helper/security.helper');
var router = express.Router();

router.get('/', async function (req, res, next) {
    try {
        res.json(await domain.getAlleGemeenten(req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

router.get('/:id',async function (req, res, next) {
    try {
        res.json(await domain.getGemeenteVoorId(req.params.id, req.query));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});

securityHelper.zetAlsAdminRoute(router);

router.post('/', async function (req, res) {
    try {
        res.status(201).json(await domain.maakGemeente(req.body));
    } catch (err) {
        res.status(500).json(error.mongooseErrorToResult(err));
    }
});


module.exports = router;
