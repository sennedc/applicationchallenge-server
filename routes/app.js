var express = require('express');
var router = express.Router();

var versieEenRouter = require('./v1/v1');
var versieTweeRouter = require('./v2/v2');

router.use('/v1', versieEenRouter);
router.use('/v2', versieTweeRouter);

module.exports = router;
